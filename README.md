# fun-box_test

Тестовое задание на вакансию фронтенд разработчика в fun-box.ru


## Installation guide

1. Clone repo or download archive and extract  

2. Enter cloned/extracted folder and install required packages
   ```
   npm install 
   ```

3. Build project
   ```
   npm run build
   ```
   Or start with watcher and open in browser
   ```
   npm run start 
   ```