var gulp = require('gulp');
var less = require('gulp-less');
var babel = require('gulp-babel');
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');
var rename = require('gulp-rename');
var cleanCSS = require('gulp-clean-css');
var del = require('del');
var pug = require('gulp-pug');
var browserSync = require('browser-sync');
var imagemin = require('gulp-imagemin');

var paths = {
    views: {
        srcAll: 'src/views/**/*.pug',
        src: 'src/views/*.pug',
        dest: 'dist/'
    },
    styles: {
        srcAll: 'src/styles/**/*.*',
        src: 'src/styles/main.less',
        dest: 'dist/assets/css/'
    },
    scripts: {
        src: 'src/scripts/**/*.js',
        dest: 'dist/assets/js/'
    },
    images: {
        src: 'src/images/**/*.*',
        dest: 'dist/assets/images/'
    },
    fonts: {
        src: 'src/fonts/*.*',
        dest: 'dist/assets/fonts/'
    }
};



gulp.task('clean', function() {
    return del('dist');
});

/*
 * Define our tasks using plain functions
 */
gulp.task('views', function () {
    return gulp.src(paths.views.src)
        .pipe(pug({pretty: true}))
        .pipe(gulp.dest(paths.views.dest))
});
gulp.task('styles', function () {
    return gulp.src(paths.styles.src)
        .pipe(less())
        .pipe(cleanCSS())
        // pass in options to the stream
        .pipe(rename({
            basename: 'main',
            suffix: '.min'
        }))
        .pipe(gulp.dest(paths.styles.dest));
});
gulp.task('scripts', function () {
    return gulp.src(paths.scripts.src, { sourcemaps: true })
        .pipe(babel({
            presets: ['@babel/env']
        }))
        .pipe(uglify())
        .pipe(concat('main.min.js'))
        .pipe(gulp.dest(paths.scripts.dest));
});
gulp.task('images', function () {
    return gulp.src(paths.images.src)
        .pipe(imagemin())
        .pipe(gulp.dest(paths.images.dest));
});
gulp.task('fonts', function () {
    return gulp.src(paths.fonts.src)
        .pipe(gulp.dest(paths.fonts.dest));
});
/*
 * You could even use `export as` to rename exported tasks
 */
gulp.task('watch', function() {
    gulp.watch(paths.views.srcAll, gulp.parallel('views'));
    gulp.watch(paths.scripts.src, gulp.parallel('scripts'));
    gulp.watch(paths.styles.srcAll, gulp.parallel('styles'));
    gulp.watch(paths.images.src, gulp.parallel('images'));
    gulp.watch(paths.fonts.src, gulp.parallel('fonts'));
});


/*
 * Specify if tasks run in series or parallel using `gulp.series` and `gulp.parallel`
 */
gulp.task('build', gulp.series('clean', gulp.parallel('views', 'styles', 'scripts', 'images', 'fonts')));

gulp.task('server', function() {
    browserSync.init({
        server: {
            baseDir: "dist"
        },
        files: ['dist/**/*.*']
    });
});


gulp.task('default', gulp.parallel('build', 'watch', 'server'));
