let cards = document.querySelectorAll('.card');
const classes = {
    default:        'card--default', 
    defaultHover:   'card--defaultHover', 
    selected:       'card--selected', 
    selectedHover:  'card--selectedHover', 
    disabled:       'card--disabled'
};
cards.forEach(card => {
    
    if (hasClass(card, classes.disabled)) return false

    let cardWrapper = card.querySelector('.card__wrapper')
    
    cardWrapper.addEventListener('mouseover', () => {
        if (hasClass(card, classes.default)) {
            addClass(card, classes.defaultHover);
        } else if (hasClass(card, classes.selected)) {
            addClass(card, classes.selectedHover);
        }
    });
    cardWrapper.addEventListener('mouseout', () => {
        removeClass(card, [classes.defaultHover, classes.selectedHover]);
        if (hasClass(card, 'select')) {
            removeClass(card, ['select', classes.default]);
            addClass(card, classes.selected);
        }
        if (hasClass(card, 'unselect')) {
            removeClass(card, ['unselect', classes.selected]);
            addClass(card, classes.default);
        }
    });
    cardWrapper.addEventListener('click', () =>{
        removeClass(card, [classes.defaultHover, classes.selectedHover])
        if (hasClass(card, classes.default)) {
            removeClass(card, [classes.default]);
            addClass(card, classes.selected);
        } else if (hasClass(card, classes.selected)) {
            removeClass(card, [classes.selected]);
            addClass(card, classes.default);
        }

    });
})

document.querySelectorAll('.select_product').forEach(link => {
    link.addEventListener('click', e => {
        e.preventDefault();
        var card = link.parentNode.parentNode.parentNode;
        removeClass(card, [classes.default]);
        addClass(card, classes.selected);
    })
})



function addClass(element, classString) {
    element.classList.add(classString);
}

function removeClass(element, classes) {
    classes.forEach(classString => element.classList.remove(classString));
}

function hasClass(element, classString) {
    return element.classList.contains(classString);
}
